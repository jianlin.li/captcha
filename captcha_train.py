import argparse
import string

import torch
import pyro
import importlib
import numpy as np
from captcha_model import captcha_model
import time
from _datetime import datetime
import json
import pyro.nn as PN
import torch.nn as TN
import tqdm
from itertools import chain

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def sample_txt_and_img(model, plate_size):
    unconditioned_model = pyro.poutine.uncondition(model)
    return unconditioned_model(plate_size=plate_size, observations={})

def test(csis, txt, img, verbose=False):
    assert len(txt) == len(img)
    csis.guide.eval()
    with torch.no_grad():
        total_correct = 0
        char_correct = 0
        total_char = 0
        noise_difference = 0
        average_ess = 0
        for i in range(len(txt)):
            posterior = csis.run(
                plate_size = 1,
                observations={
                    "obs_0" : img[i]
                }
            )
            average_ess += csis.get_ESS().item()
            marginal_ret = pyro.infer.EmpiricalMarginal(posterior)
            _ret = marginal_ret().squeeze(0)
            _txt_ret = _ret[0:args.MAX_N]

            def tensor2str(t):
                str = ""
                for i in range(len(t)):
                    if t[i] > 0:
                        str += chr(int(t[i]))
                return str

            p_text = tensor2str(_txt_ret)
            t_text = tensor2str(txt[i])

            correct = sum([int(p_char == t_char) for p_char, t_char in zip(p_text, t_text)])

            if verbose:
                print(f"| Predicted Text: {p_text} | Actual Text: {t_text} | Correct: {correct}")

            total_correct += int(p_text==t_text)
            char_correct += correct
            total_char += len(t_text)

        num_test_samples = len(txt)
        average_ess = average_ess / num_test_samples
        accuracy = total_correct / num_test_samples
        char_accuracy = char_correct / total_char

        print(
            "Total correct:",
            total_correct,
            "accuracy:{}/{}=".format(total_correct, num_test_samples),
            accuracy,
            "char_accuracy:{}/{}=".format(char_correct, total_char),
            char_accuracy,
            "AVG ESS:",
            average_ess,
        )
    csis.guide.train()
    return accuracy, char_accuracy, average_ess

def main(args):

    pyro.set_rng_seed(0)
    torch.manual_seed(0)
    np.random.seed(0)
    pyro.clear_param_store()

    print()
    print("==========")
    print(f"[main] smoke test = {args.smoke}")

    if args.smoke:
        args.n_steps = 8
        args.num_inference_samples = 10
        args.training_batch_size = 10
        args.validation_batch_size = 10
        args.plate_size = 2
        args.scheduler_period = 5
        args.validation_period = 5

    print(f"[main] {args}")

    start_date_time = datetime.now().strftime("%Y_%m_%d_%H_%M")
    start_time = time.time()
    print(f"[main] start at {start_date_time}")

    decive = torch.device(args.device)

    model = captcha_model(
        device=decive,
        captchaHeight=args.captchaHeight,
        captchaWidth=args.captchaWidth,
        captchaMarginX=args.captchaMarginX,
        captchaMarginY=args.captchaMarginY,
        MAX_N=args.MAX_N,
        MIN_N=args.MIN_N,
        char_dict=args.char_dict
    )

    guidelib = importlib.import_module(args.guide)
    guide = guidelib.captcha_guide(
        device=decive,
        captchaHeight=args.captchaHeight,
        captchaWidth=args.captchaWidth,
        captchaMarginX=args.captchaMarginX,
        captchaMarginY=args.captchaMarginY,
        MAX_N=args.MAX_N,
        MIN_N=args.MIN_N,
        char_dict=args.char_dict,
        hidden_dim_1 =args.hidden_dim_1,
        hidden_dim_2=args.hidden_dim_2,
        hidden_shape=args.hidden_shape,
        img_obs_shape=args.img_obs_shape
    )
    class ObsEmbedding(PN.PyroModule):
        def __init__(self, guide, device):
            super().__init__(name="captcha_obsembeding")
            self.device = device
            self._cnn = PN.PyroModule[TN.Sequential](
                PN.PyroModule[TN.Conv2d](1,64,3),
                PN.PyroModule[TN.ReLU](),
                PN.PyroModule[TN.MaxPool2d](2,2),
                PN.PyroModule[TN.Conv2d](64,64,3),
                PN.PyroModule[TN.ReLU](),
                PN.PyroModule[TN.MaxPool2d](2,2),
                PN.PyroModule[TN.Conv2d](64,64,3),
                PN.PyroModule[TN.ReLU](),
                PN.PyroModule[TN.MaxPool2d](2,2)
            ).to(self.device)
            self._fnn = PN.PyroModule[TN.Sequential](
                PN.PyroModule[TN.Linear](1280, args.img_obs_shape)
            ).to(self.device)
            self.guide = guide

        def embedding(self, img):
            img = img.unsqueeze(0).unsqueeze(0)
            feature = self._cnn(img)
            emb = self._fnn(feature.flatten())
            return emb

        def wrapped_guide(self, plate_size=None, observations=None):
            observations_emb = {
                f"obs_{i}" : self.embedding(observations[f"obs_{i}"])
                for i in range(plate_size)
            }
            return self.guide(plate_size=plate_size, observations=observations_emb)

        def __call__(self, *args, **kwargs):
            return self.wrapped_guide(*args, **kwargs)
    wrapped_guide = ObsEmbedding(guide=guide,device=decive)
    print("[main] guide imported:", args.guide)

    capiticy = count_parameters(wrapped_guide)
    capiticy_guide = count_parameters(guide)
    capiticy_cnn = count_parameters(wrapped_guide._cnn)
    capiticy_fnn = count_parameters(wrapped_guide._fnn)
    print(f"[main] capiticy = {capiticy} = {capiticy_guide} + {capiticy_cnn + capiticy_fnn}")

    scheduler = pyro.optim.ExponentialLR({
        'optimizer': torch.optim.Adam,
        'optim_args': {'lr': args.lr},
        'gamma': args.gamma
    })

    csis = pyro.infer.CSIS(
        model.model,
        wrapped_guide,
        scheduler,
        num_inference_samples=args.num_inference_samples,
        training_batch_size=args.training_batch_size,
        validation_batch_size=args.validation_batch_size,
    )

    val_loss_begin = csis.validation_loss(plate_size=args.plate_size, observations={})
    print(f"[main] val_loss_begin = {val_loss_begin}")

    def make_val_set(csis):
        assert csis.validation_batch is not None
        with torch.no_grad():
            returns = [trace.nodes["_RETURN"]["value"] for trace in csis.validation_batch]
            flattened = torch.cat(returns)
            txt = flattened[:,:args.MAX_N]
            img = flattened[:,args.MAX_N:].reshape(-1,32,100)
        return txt, img

    txt, img = make_val_set(csis)

    loss_curve = []
    val_accuracy = []
    val_char_accuracy = []
    val_avg_ess = []
    for step in tqdm.trange(args.n_steps):
        loss = csis.step(plate_size=args.plate_size,observations={})
        loss_curve.append(loss)
        if (step + 1) % args.validation_period == 0:
            print(f"[validation] step/n_steps : {step}/{args.n_steps}")
            accuracy, char_accuracy, avg_ess = test(csis, txt, img)
            val_accuracy.append(accuracy)
            val_char_accuracy.append(char_accuracy)
            val_avg_ess.append(avg_ess)
        if (step + 1) % args.scheduler_period == 0:
            scheduler.step()
            p = next(iter(scheduler.optim_objs))
            lr = scheduler.optim_objs[p].optimizer.param_groups[0]["lr"]
            print(f"[scheduler update] lr = {lr}")

    val_loss_final = csis.validation_loss(plate_size=args.plate_size, observations={})
    print(f"[main] val_loss_end = {val_loss_final}")
    ess = csis.get_ESS().item()

    test_stacked = sample_txt_and_img(model, args.plate_size)
    test_txt = test_stacked[:,:args.MAX_N]
    test_img = test_stacked[:,args.MAX_N:].reshape(-1,32,100)

    test_accuracy, test_char_accuracy, test_ess = test(
            csis, test_txt, test_img
    )

    end_date_time = datetime.now().strftime("%Y_%m_%d_%H_%M")
    end_time = time.time()
    print(f"[main] finished at {end_date_time}")

    with open(f"result_{args.guide}_{args.note}_{start_date_time}.json", "w") as f:
        res_dict = {
            "args" : vars(args),
            "capicity": capiticy,
            "capiticy_cnn" : capiticy_cnn,
            "capiticy_fnn" : capiticy_fnn,
            "capiticy_guide" : capiticy_guide,
            "val_loss_begin": val_loss_begin,
            "val_loss_final": val_loss_final,
            "ess": ess,
            "loss": loss_curve,
            "test_accuracy": test_accuracy,
            "test_char_accuracy": test_char_accuracy,
            "test_ess": test_ess,
            "val_accuracy": val_accuracy,
            "val_char_accuracy": val_char_accuracy,
            "val_avg_ess": val_avg_ess,
            "time" : end_time - start_time
        }
        json.dump(res_dict, f, indent=4, sort_keys=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Input the guide file name")
    parser.add_argument("--note", type=str, default="")
    parser.add_argument("--guide", type=str, required=True)
    parser.add_argument("--device", type=str, required=True)
    parser.add_argument("--smoke", action='store_true', default=False)

    parser.add_argument("--plate_size", type=int, default=32)
    parser.add_argument("--captchaHeight", type=int, default=32)
    parser.add_argument("--captchaWidth", type=int, default=100)
    parser.add_argument("--captchaMarginX", type=int, default=4)
    parser.add_argument("--captchaMarginY", type=int, default=4)
    parser.add_argument("--MAX_N", type=int, default=4)
    parser.add_argument("--MIN_N", type=int, default=2)
    parser.add_argument("--char_dict", type=str, default=string.digits)

    parser.add_argument("--lr", type=float, default=1e-3)
    parser.add_argument("--gamma", type=float, default=0.8)
    parser.add_argument("--n_steps", type=int, default=200_000)
    parser.add_argument("--scheduler_period", type=int, default=10_000)
    parser.add_argument("--validation_period", type=int, default=10_000)
    parser.add_argument("--num_inference_samples", type=int, default=100)
    parser.add_argument("--training_batch_size", type=int, default=16)
    parser.add_argument("--validation_batch_size", type=int, default=1000)

    parser.add_argument("--img_obs_shape", type=int, default=680)
    parser.add_argument("--hidden_dim_1", type=int, default=520)
    parser.add_argument("--hidden_dim_2", type=int, default=320)
    parser.add_argument("--hidden_shape", type=int, default=520)

    parser.add_argument("--test_size", type=int, default=1000)
    parser.add_argument("--n_trails", type=int, default=1)
    parser.add_argument("--sentence_emb_shape", type=int, default=100) # used in fidelia only, need to manually change in utils as well
    args = parser.parse_args()
    main(args)