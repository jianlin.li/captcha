import string
import pyro
import torch
import pyro.distributions as dist
from claptcha.claptcha import Claptcha
from PIL import Image
import numpy as np
from _datetime import datetime

class captcha_model(pyro.nn.PyroModule):
    def __init__(
            self,
            device=torch.device("cpu"),
            captchaHeight = 32,
            captchaWidth = 100,
            captchaMarginX = 4,
            captchaMarginY = 4,
            MAX_N = 4,
            MIN_N = 2,
            char_dict = string.digits
    ):
        super().__init__(name="captcha_model")
        self.captchaHeight = captchaHeight
        self.captchaWidth  = captchaWidth
        self.captchaMarginX = captchaMarginX
        self.captchaMarginY = captchaMarginY
        self.device = device
        self.char_dict = char_dict
        self.MAX_N = MAX_N
        self.MIN_N = MIN_N

    def model(
            self,
            plate_size=1,
            observations={}
    ):
        ret = []
        for data_idx in pyro.plate("data", plate_size):
            num = pyro.sample(f"num_{data_idx}", dist.Categorical(logits=torch.ones(self.MAX_N-self.MIN_N+1).to(self.device)))
            num_char = self.MIN_N + num

            def model_char(i, num_char):
                if i < num_char:
                    char_i = pyro.sample(f"char_{data_idx}_{i}", dist.Categorical(logits=torch.ones(len(self.char_dict)).to(self.device)))
                    rhs_chars = model_char(i + 1, num_char)
                    return self.char_dict[char_i] + rhs_chars
                else:
                    return ""

            text = model_char(i=0, num_char=num_char)

            def render_image(chars, noise, fonts="fonts/FreeSans.ttf", resample=Image.BILINEAR, save=False):
                size = (self.captchaWidth, self.captchaHeight)
                margin = (self.captchaMarginX, self.captchaMarginY)
                pad_spaces = self.MAX_N - len(chars)
                space = " " * (pad_spaces // 2)
                chars = space + chars + space
                render = Claptcha(chars, fonts, size, margin, resample=resample, noise=float(noise))

                _ , rendered_image = render.image
                rendered_image = np.array(rendered_image)[:,:,0] # the generator is gray scale, only keep one channel is enough
                rendered_image = np.subtract(np.divide(rendered_image, 255), 0.5)
                rendered_image = torch.from_numpy(rendered_image).float().to(self.device)

                if save:
                    text, _ = render.write(f"captcha_{datetime.now().strftime('%Y_%m_%d_%H:%M:%S')}.png")

                return rendered_image

            concentration0 = torch.tensor(1.0).to(self.device)
            concentration1 = torch.tensor(3.0).to(self.device)

            noise = pyro.sample(f"noise_{data_idx}", dist.Beta(concentration0,concentration1))
            gen_image = render_image(text, noise)
            obs_image = observations.get(f"obs_{data_idx}", torch.tensor([]))
            pyro.sample(f"obs_{data_idx}", dist.Normal(gen_image, 1e-5), obs=obs_image)
            text_ASCII_pad = torch.tensor([ord(c) for c in text] + [-1] * (self.MAX_N - len(text))).to(self.device)
            gen_image_flattened = gen_image.flatten()
            ret.append(torch.cat([text_ASCII_pad, gen_image_flattened]))

        return torch.stack(ret)

    def __call__(self, *args, **kwargs):
        return self.model(*args, **kwargs)

if __name__ == '__main__':
    model = captcha_model(
        # captchaHeight=24,
        # captchaWidth=60,
        # captchaMarginX=0,
        # captchaMarginY=0
    ).model
    unconditioned_model = pyro.poutine.uncondition(model)
    unconditioned_model(2)
