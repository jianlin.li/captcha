import torch as T
import torch.distributions as TD
import torch.distributions.constraints as TDC
import torch.nn as TN
import torch.nn.functional as TNF
import pyro as P
import pyro.infer as PI
import pyro.optim as PO
import pyro.nn as PN
import pyro
import pyro.distributions as PD
import torch
import string

class captcha_guide(pyro.nn.PyroModule):
    def __init__(
            self,
            device=torch.device("cpu"),
            captchaHeight = 32,
            captchaWidth = 100,
            captchaMarginX = 4,
            captchaMarginY = 4,
            char_dict = string.digits,
            hidden_dim_1 = 154,
            hidden_dim_2 = 64,
            hidden_shape = 128,
            img_obs_shape = 640,
            MAX_N = 4,
            MIN_N = 2,
            **kwargs
    ):
        super().__init__(name="captcha_fidelia")

        self.captchaHeight = captchaHeight
        self.captchaWidth  = captchaWidth
        self.captchaMarginX = captchaMarginX
        self.captchaMarginY = captchaMarginY
        self.device = device
        self.char_dict = char_dict
        self.MAX_N = MAX_N
        self.MIN_N = MIN_N

        self._nn_model_char_char_i = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](img_obs_shape + hidden_shape, hidden_dim_1),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_1, hidden_dim_2),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_2, 10)
        ).to(self.device)

        self._nn_rhs = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](hidden_shape + 1, hidden_dim_1),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_1, hidden_shape)
        ).to(self.device)

        self._nn_model_noise = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](img_obs_shape, hidden_dim_1),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_1, hidden_dim_2),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_2, 2)
        ).to(self.device)

        self._nn_model_num_char = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](img_obs_shape, hidden_dim_1),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_1, hidden_dim_2),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_2, self.MAX_N-self.MIN_N+1)
        ).to(self.device)

        self._nn_text = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](img_obs_shape + 1, hidden_dim_1),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](hidden_dim_1, hidden_shape)
        ).to(self.device)

    def guide(
            self,
            plate_size=1,
            observations={}
    ):
        ret = []
        for data_idx in pyro.plate("data", plate_size):
            obs = observations[f"obs_{data_idx}"]
            _noise_params = self._nn_model_noise(obs)
            noise = P.sample(f'noise_{data_idx}', PD.Beta(TNF.softplus(_noise_params[0]), TNF.softplus(_noise_params[1])))
            _num_char_params = self._nn_model_num_char(obs)
            num = P.sample(f'num_{data_idx}', PD.Categorical(logits=_num_char_params))
            num_char = self.MIN_N + num
            _hidden =  self._nn_text(T.cat([obs, num_char.unsqueeze(0).to(self.device)]))

            def model_char(obs, _hidden, i, num_char):
                if i < num_char:
                    _char_i_params = self._nn_model_char_char_i(T.cat([obs, _hidden]))
                    char_i = P.sample(f'char_{data_idx}_{i}', PD.Categorical(logits=_char_i_params))
                    _new_hidden = self._nn_rhs(T.cat([_hidden, char_i.unsqueeze(0)]))
                    rhs = model_char(obs, _new_hidden, i + 1, num_char)
                    return self.char_dict[char_i] + rhs
                else:
                    return ''

            text = model_char(obs, _hidden, 0, num_char)
            text_ASCII_pad = torch.tensor([ord(c) for c in text] + [-1] * (self.MAX_N - len(text)))
            ret.append(text_ASCII_pad)

        return torch.stack(ret)

    def __call__(self, *args, **kwargs):
        return self.guide(*args, **kwargs)
