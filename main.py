# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pyro
import pyro.distributions as dist

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    def model():
        for i in pyro.plate("data", 2):
            print(pyro.sample("x", dist.Normal(0,1)))
    with pyro.poutine.trace() as t:
        model()
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
